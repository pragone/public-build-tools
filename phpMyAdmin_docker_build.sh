#!/bin/bash

set -x
docker --host $DOCKER_HOST build --tag phpMyAdmin_arm:${GO_REVISION:0:7} --tag phpMyAdmin_arm:latest apache
